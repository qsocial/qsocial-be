<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for navigation.
    | You are free to modify these language lines according to your
    | application's requirements.
    |
    */

    'back' => 'Go Back',
];
