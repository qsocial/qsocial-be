<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various error
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | HTTP Errors
    |--------------------------------------------------------------------------
    */
    'http_401' => 'HTTP 401: Unauthorized.',
    'http_404' => 'HTTP 404: Not Found.',

    'http_500' => 'HTTP 500: Internal Server Error.',
];
