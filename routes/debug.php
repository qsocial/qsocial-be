<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Debug Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "debug" middleware group. Now create something great!
|
*/

/** Error pages. */
Route::group(['name' => 'Error Pages'], function () {
    Route::view('401', 'errors.401')->name('HTTP401 Error Page');
    Route::view('404', 'errors.404')->name('HTTP404 Error Page');
    Route::view('500', 'errors.500')->name('HTTP500 Error Page');
});
