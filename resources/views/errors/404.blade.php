@extends('errors.__layout')

@section('title') {{ __('errors.http_404') }} @endsection
@section('error') {{ __('errors.http_404') }} @endsection
