@extends('errors.__layout')

@section('title') {{ __('errors.http_500') }} @endsection
@section('error') {{ __('errors.http_500') }} @endsection
