@extends('errors.__layout')

@section('title') {{ __('errors.http_401') }} @endsection
@section('error') {{ __('errors.http_401') }} @endsection
