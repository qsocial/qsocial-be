@extends('template')
@section('body')
    <div class="uk-position-center uk-text-center">
            <h1>@yield('error')</h1>
            <button class="uk-button uk-button-primary" onclick="history.back()">{{ __('nav.back') }}</button>
    </div>
@endsection
