<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/uikit.min.css">
    <script src="/assets/uikit.min.js"></script>
    <script src="/assets/uikit-icons.min.js"></script>
    <title>@yield('title')</title>
</head>
<body>
    @yield('body')
</body>
</html>
